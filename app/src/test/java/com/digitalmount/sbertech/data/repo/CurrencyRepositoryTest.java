package com.digitalmount.sbertech.data.repo;

import com.digitalmount.sbertech.data.database.IDatabase;
import com.digitalmount.sbertech.data.model.CurrencyModel;
import com.digitalmount.sbertech.data.network.INetworkUtils;
import com.digitalmount.sbertech.data.network.OnLoadCallBack;

import java.util.List;

/**
 * Created by Vadim Karpenko on 07.12.2017.
 */
public class CurrencyRepositoryTest implements ICurrencyRepository {


    private final IDatabase database;
    private final INetworkUtils networkUtils;

    public CurrencyRepositoryTest(IDatabase database, INetworkUtils networkUtils) {

        this.database = database;
        this.networkUtils = networkUtils;
    }

    @Override
    public void getCurrencyModels(OnLoadCallBack onDBSuccessLoad) {

        if (networkUtils.connectionEnabled())
            onDBSuccessLoad.onSuccessLoad(database.getCurrencyModels());
        else if (database.getCurrencyModels().size() > 0) {
            onDBSuccessLoad.onSuccessLoad(database.getCurrencyModels());
        } else {
            onDBSuccessLoad.onError(new String());
        }
    }

    @Override
    public void addCurrencyModels(List<CurrencyModel> data) {
    }
}