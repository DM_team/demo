package com.digitalmount.sbertech.data.network;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertNotNull;
import static org.mockito.Mockito.mock;

/**
 * Created by Vadim Karpenko on 06.12.2017.
 */

@RunWith(MockitoJUnitRunner.class)
public class CurrencyLoaderTest {

    private static final String TAG = "CurrencyLoader";
    private CurrencyLoader loader;


    @Before
    public void setUp() throws Exception {
        loader = mock(CurrencyLoader.class);
    }

    @Test
    public void testCreated() throws Exception {
        assertNotNull(loader);
    }


    @Test
    public void parseFailSuccess() throws Exception {
        boolean ff = loader.parse("");
        assertFalse(ff);
    }
}