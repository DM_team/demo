package com.digitalmount.sbertech.data.database;

import com.digitalmount.sbertech.data.model.CurrencyModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Vadim Karpenko on 07.12.2017.
 */
public class DatabaseTest implements IDatabase {


    private int size;

    public DatabaseTest(int size) {
        this.size = size;
    }

    @Override
    public List<CurrencyModel> getCurrencyModels() {

        ArrayList<CurrencyModel> list = new ArrayList<>();

        for (int i = 0; i < size; i++) {
            list.add(new CurrencyModel());
        }
        return list;
    }

    @Override
    public void updateCurrencyModels(List<CurrencyModel> data) {

    }

    @Override
    public void addValueModel(CurrencyModel model) {

    }

    @Override
    public int getSize() {
        return size;
    }


    @Override
    public void clearDb() {

    }

}