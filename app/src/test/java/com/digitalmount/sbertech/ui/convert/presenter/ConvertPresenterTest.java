package com.digitalmount.sbertech.ui.convert.presenter;

import com.digitalmount.sbertech.data.database.DatabaseTest;
import com.digitalmount.sbertech.data.repo.CurrencyRepository;
import com.digitalmount.sbertech.data.repo.CurrencyRepositoryTest;
import com.digitalmount.sbertech.data.repo.ICurrencyRepository;
import com.digitalmount.sbertech.ui.convert.view.IMainView;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static junit.framework.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.validateMockitoUsage;
import static org.mockito.Mockito.verify;

/**
 * Created by Vadim Karpenko on 05.12.2017.
 */

@RunWith(MockitoJUnitRunner.class)
public class ConvertPresenterTest {

    @Captor
    private
    ArgumentCaptor<ConvertPresenter.CallBack> callbackCaptor;

    @Mock
    private ConvertPresenter mPresenter;

    @Mock
    private IMainView view;
    private ICurrencyRepository testRepository;

    @Before
    public void setUp() throws Exception {
        view = mock(IMainView.class);
        testRepository = mock(CurrencyRepository.class);
        mPresenter = new ConvertPresenter(view, testRepository);
    }

    @Test
    public void testCreated() throws Exception {
        assertNotNull(mPresenter);
    }


    @Test
    public void loadFailDBZeroConnectionsFails() throws Exception {

        ICurrencyRepository repository = new CurrencyRepositoryTest(new DatabaseTest(0), () -> false);
        ConvertPresenter mPresenter2 = new ConvertPresenter(view, repository);

        ConvertPresenter.CallBack callback = mock(ConvertPresenter.CallBack.class);
        mPresenter2.askCurrency(callback);


        verify(view, times(1)).showError(anyString());
        verify(view, times(1)).showLoading();
        verify(view, times(1)).hideLoading();
        verify(callback, times(0)).onLoad(any());


    }

    @Test
    public void loadSuccessDBCountConnectionsFails() throws Exception {

        ICurrencyRepository repository = new CurrencyRepositoryTest(new DatabaseTest(5), () -> false);
        ConvertPresenter mPresenter2 = new ConvertPresenter(view, repository);

        ConvertPresenter.CallBack callback = mock(ConvertPresenter.CallBack.class);
        mPresenter2.askCurrency(callback);

        verify(view, times(0)).showError(anyString());
        verify(view, times(0)).showError();
        verify(callback, times(1)).onLoad(any());

        verify(view, times(1)).showLoading();
        verify(view, times(1)).hideLoading();

    }

    @Test
    public void loadSuccessDBZeroConnectionsSuccess() throws Exception {

        ICurrencyRepository repository = new CurrencyRepositoryTest(new DatabaseTest(0), () -> true);
        ConvertPresenter mPresenter2 = new ConvertPresenter(view, repository);

        ConvertPresenter.CallBack callback = mock(ConvertPresenter.CallBack.class);
        mPresenter2.askCurrency(callback);


        verify(view, times(1)).showLoading();
        verify(view, times(1)).hideLoading();
        verify(callback, times(1)).onLoad(any());


        verify(view, times(0)).showError(anyString());
        verify(view, times(0)).showError();

    }

    @Test
    public void loadFailDBConnectionsSuccess() throws Exception {

        ICurrencyRepository repository = new CurrencyRepositoryTest(new DatabaseTest(5), () -> true);
        ConvertPresenter mPresenter2 = new ConvertPresenter(view, repository);


        ConvertPresenter.CallBack callback = mock(ConvertPresenter.CallBack.class);
        mPresenter2.askCurrency(callback);


        verify(view, times(1)).showLoading();
        verify(view, times(1)).hideLoading();
        verify(callback, times(1)).onLoad(any());


        verify(view, times(0)).showError(anyString());
        verify(view, times(0)).showError();

    }

    @After
    public void validate() {
        validateMockitoUsage();
    }
}