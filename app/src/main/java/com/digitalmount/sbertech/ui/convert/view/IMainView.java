package com.digitalmount.sbertech.ui.convert.view;

import android.widget.Button;

/**
 * Created by Vadim Karpenko on 29.11.2017.
 */

public interface IMainView {
    void showError();

    void testMethod();

    void showError(String s);

    void showAlertDialog(String[] list, Button button);

    void showLoading();

    void hideLoading();

    public interface OnUpdateResult {
        void onUpdate(String format);
    }
}
