package com.digitalmount.sbertech.ui.convert.presenter;

import android.widget.Button;

import com.digitalmount.sbertech.ui.convert.view.IMainView;

/**
 * Created by vadimkarpenko on 30.11.17.
 */

public interface IConvertPresenter {
    void showResult(String input, IMainView.OnUpdateResult output, boolean showError);

    void setFirstIndex(int pos);

    void setSecondIndex(int pos);

    void askCurrency(ConvertPresenter.CallBack callBack);

    void swapButton(Button btnFrom, Button btnTo);

    void askCurrency2(Button btnFrom);
}
