package com.digitalmount.sbertech.ui.convert;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.digitalmount.sbertech.R;
import com.digitalmount.sbertech.data.database.Database;
import com.digitalmount.sbertech.data.database.DatabaseHelper;
import com.digitalmount.sbertech.data.network.NetworkUtils;
import com.digitalmount.sbertech.data.repo.CurrencyRepository;
import com.digitalmount.sbertech.data.repo.ICurrencyRepository;
import com.digitalmount.sbertech.ui.convert.presenter.ConvertPresenter;
import com.digitalmount.sbertech.ui.convert.presenter.IConvertPresenter;
import com.digitalmount.sbertech.ui.convert.view.IMainView;


public class ConvertActivity extends AppCompatActivity implements IMainView {

    private static final String TAG = "ConvertActivity";
    private IConvertPresenter presenter;

    private Button btnFrom;
    private Button btnTo;
    private EditText from;
    private EditText to;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_convert);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(getResources().getString(R.string.money_converter));

        btnFrom = (Button) findViewById(R.id.first_selectable);
        btnTo = (Button) findViewById(R.id.second_selectable);
        ImageView btnChange = (ImageView) findViewById(R.id.btnChange);

        from = (EditText) findViewById(R.id.first_input);
        to = (EditText) findViewById(R.id.second_input);


        ICurrencyRepository repository = CurrencyRepository.getInstance(new Database(new DatabaseHelper(this)), new NetworkUtils(this));


        presenter = new ConvertPresenter(this, repository);

//        btnFrom.setOnClickListener(v -> presenter.askCurrency2(btnFrom));
//        btnTo.setOnClickListener(v -> presenter.askCurrency2(btnTo));


        btnFrom.setOnClickListener(v ->
                presenter.askCurrency(
                        strings -> showAlertDialog(strings, btnFrom)));
        btnTo.setOnClickListener(v ->
                presenter.askCurrency(
                        strings -> showAlertDialog(strings, btnTo)));


        btnChange.setOnClickListener(v -> {
            from.setText(Double.parseDouble(String.valueOf(to.getText())) + "");
            presenter.swapButton(btnFrom, btnTo);
            showResult(false);
        });


        from.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (!TextUtils.isEmpty(s))
                    showResult(false);

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


    }

    @Override
    public void showAlertDialog(String[] list, Button button) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.from_string);
        builder.setItems(list, (dialog, item) -> onValueSelected(list[item], button, item));
        AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public void showLoading() {

        if (progressDialog == null) {
            progressDialog = getDialog(this);
        }

        if (!progressDialog.isShowing())
            progressDialog.show();

    }

    @Override
    public void hideLoading() {
        if (progressDialog != null)
            if (progressDialog.isShowing())
                progressDialog.dismiss();
    }

    public ProgressDialog getDialog(Context context) {
        ProgressDialog progressDialog = new ProgressDialog(context, R.style.MyTheme);
        progressDialog.setCancelable(false);
        progressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Large);


        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            Drawable drawable = new ProgressBar(context).getIndeterminateDrawable().mutate();
            drawable.setColorFilter(ContextCompat.getColor(context, R.color.colorAccent),
                    PorterDuff.Mode.SRC_IN);
            progressDialog.setIndeterminateDrawable(drawable);
        }

        return progressDialog;
    }

    private void onValueSelected(String text, Button button, int index) {
        button.setText(text);


        if (button.getId() == btnFrom.getId()) {
            presenter.setFirstIndex(index);
        }
        if (button.getId() == btnTo.getId()) {
            presenter.setSecondIndex(index);

        }
        if (!TextUtils.isEmpty(from.getText()))
            showResult(false);

    }

    private void showResult(boolean showError) {
//        presenter.showResult(from, to, showError);
        presenter.showResult(from.getText().toString(), format -> to.setText(format), showError);


    }

    @Override
    public void showError() {
        Toast.makeText(this, "Не выбрана валюта", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void testMethod() {
    }

    @Override
    public void showError(String s) {
        Toast.makeText(this, s, Toast.LENGTH_SHORT).show();

    }
}
