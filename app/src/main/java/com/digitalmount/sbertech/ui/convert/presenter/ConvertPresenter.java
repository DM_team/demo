package com.digitalmount.sbertech.ui.convert.presenter;

import android.widget.Button;

import com.digitalmount.sbertech.data.model.CurrencyModel;
import com.digitalmount.sbertech.data.network.OnLoadCallBack;
import com.digitalmount.sbertech.data.repo.ICurrencyRepository;
import com.digitalmount.sbertech.ui.convert.view.IMainView;
import com.digitalmount.sbertech.utils.Converter;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by vadimkarpenko on 30.11.17.
 */

public class ConvertPresenter implements IConvertPresenter {
    private final IMainView view;
    private final ICurrencyRepository repository;

    private int firstPos = -1;
    private int secondPos = -1;

    @Override
    public void showResult(String input, IMainView.OnUpdateResult onUpdateResult, boolean showError) {


        if (firstPos < 0 || secondPos < 0) {

            if (showError)
                view.showError();
            return;
        }


        view.showLoading();
        repository.getCurrencyModels(new OnLoadCallBack() {
            @Override
            public void onSuccessLoad(List<CurrencyModel> valueModels) {
                view.hideLoading();
                onDBLoad(input, onUpdateResult, valueModels);
            }

            @Override
            public void onError(String s) {
                view.hideLoading();
                view.showError(s);
            }
        });

    }

    private void onDBLoad(String input, IMainView.OnUpdateResult output, List<CurrencyModel> data) {
        CurrencyModel from = data.get(firstPos);
        CurrencyModel to = data.get(secondPos);

        double inputValue = Double.parseDouble(input);
        double result = Converter.solve(inputValue, from, to);

        output.onUpdate(String.format(Locale.ENGLISH, "%2f", result));

    }

    @Override
    public void setFirstIndex(int pos) {
        this.firstPos = pos;
    }

    @Override
    public void setSecondIndex(int pos) {
        this.secondPos = pos;
    }

    @Override
    public void askCurrency(CallBack callBack) {
        List<String> array = new ArrayList<>();

        view.showLoading();
        repository.getCurrencyModels(new OnLoadCallBack() {
            @Override
            public void onSuccessLoad(List<CurrencyModel> valueModels) {
                for (CurrencyModel model : valueModels) {
                    array.add(model.getName());
                }
                view.hideLoading();
                callBack.onLoad(array.toArray(new String[array.size()]));

            }

            @Override
            public void onError(String s) {
                view.hideLoading();
                view.showError(s);
            }
        });


    }

    @Override
    public void askCurrency2(Button btn) {
        List<String> array = new ArrayList<>();

        repository.getCurrencyModels(new OnLoadCallBack() {
            @Override
            public void onSuccessLoad(List<CurrencyModel> valueModels) {
                for (CurrencyModel model : valueModels) {
                    array.add(model.getName());
                }
                view.showAlertDialog(array.toArray(new String[array.size()]), btn);

            }

            @Override
            public void onError(String s) {
                view.showError(s);
            }
        });


    }

    @Override
    public void swapButton(Button btnFrom, Button btnTo) {
        if (firstPos >= 0 && secondPos >= 0) {

            CharSequence temp = btnFrom.getText();
            btnFrom.setText(btnTo.getText());
            btnTo.setText(temp);

            int tempPos = firstPos;
            firstPos = secondPos;
            secondPos = tempPos;
        }
    }


    public ConvertPresenter(IMainView view, ICurrencyRepository repository) {
        this.view = view;
        this.repository = repository;
    }

    public interface CallBack {
        void onLoad(String[] strings);
    }
}
