package com.digitalmount.sbertech.data.database;

import android.provider.BaseColumns;

/**
 * Created by vadimkarpenko on 30.11.17.
 */

class DatabaseContract implements BaseColumns {
    public final static String TABLE_NAME = "VALUTES";
    public final static String COLUMN_NUM = "NUM_CODE";
    public final static String COLUMN_CODE = "CODE";
    public final static String COLUMN_NAME = "NAME";
    public final static String COLUMN_NOMINAL = "NOMINAL";
    public final static String COLUMN_VALUE = "VALUE";
    public final static String COLUMN_ID = "ID";

    public final static String CREATE_TABLE = String.format("CREATE TABLE %s (%s INTEGER PRIMARY KEY, " +
                    "%s TEXT, %s TEXT, %s TEXT, %s TEXT, %s TEXT, %s TEXT) ", TABLE_NAME, _ID,
            COLUMN_NAME, COLUMN_CODE, COLUMN_NOMINAL, COLUMN_VALUE, COLUMN_ID, COLUMN_NUM);

    public final static String DROP_TABLE = String.format("DROP TABLE IF EXISTS %s", TABLE_NAME);

}
