package com.digitalmount.sbertech.data.network;


import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;

import com.digitalmount.sbertech.data.model.CurrencyCourse;
import com.digitalmount.sbertech.data.model.CurrencyModel;

import org.simpleframework.xml.core.Persister;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.net.URL;
import java.util.List;


/**
 * Created by vadimkarpenko on 30.11.17.
 */

class CurrencyLoader {
    private static final String DEFAULT_CHARSET = "windows-1251";
    private static final String TAG = "CurrencyLoader";
    private final Handler errorHandler;
    private OnNetworkLoad onNetworkLoad;
    private final Handler handler;

    public CurrencyLoader() {
        handler = new Handler(msg -> {
            onNetworkLoad.onLoad((List<CurrencyModel>) msg.obj);
            return true;
        });

        errorHandler = new Handler(msg -> {
            onNetworkLoad.onError((String) msg.obj);
            return true;
        });
    }

    private void load() throws Exception {
        String url = SettingClass.API;
        InputStream response = new URL(url).openStream();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(response, DEFAULT_CHARSET));

        String responseBody = "";
        String line;
        do {
            line = bufferedReader.readLine();
            responseBody += line;
        } while (line != null);


        parse(responseBody);

//
    }

    public boolean parse(String responseBody) {

        System.out.print("responseBody = " + responseBody);

        Reader reader = new StringReader(responseBody);
        Persister deserializer = new Persister();

        CurrencyCourse course = null;

        try {
            course = deserializer.read(CurrencyCourse.class, reader, false);
        } catch (Exception th) {
            th.printStackTrace();
            sendError(getMessage("Parsing error"));
            return false;
        }

        sendResult(course.getValueModels());
        return true;
    }

    public void sendResult(List<CurrencyModel> course) {
        handler.sendMessage(getMessage(course));
    }

    public void sendError(Message message) {
        errorHandler.sendMessage(message);
    }

    private Message getMessage(Object object) {
        Message msg = new Message();
        msg.obj = object;
        return msg;

    }

    public void getModelsList(OnNetworkLoad networkLoad) {
        this.onNetworkLoad = networkLoad;
        HandlerThread thread = new HandlerThread("handleThread") {

            public void run() {
                try {
                    load();
                } catch (Exception e) {
                    e.printStackTrace();
                    sendError(getMessage("Network error. Can't load currency list"));
                }
            }
        };
        thread.start();
    }

}
