package com.digitalmount.sbertech.data.repo;

import com.digitalmount.sbertech.data.model.CurrencyModel;
import com.digitalmount.sbertech.data.network.OnLoadCallBack;

import java.util.List;

/**
 * Created by vadimkarpenko on 30.11.17.
 */

public interface ICurrencyRepository {
    void getCurrencyModels(OnLoadCallBack onDBSuccessLoad);
    void addCurrencyModels(List<CurrencyModel> data);
}
