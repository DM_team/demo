package com.digitalmount.sbertech.data.network;

import android.content.Context;
import android.net.ConnectivityManager;


/**
 * Created by Vadim Karpenko on 06.12.2017.
 */

public class NetworkUtils implements INetworkUtils {
    private Context context;

    public NetworkUtils(Context context) {

        this.context = context;
    }

    @Override
    public boolean connectionEnabled() {
        ConnectivityManager manager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        return manager.getActiveNetworkInfo() != null && manager.getActiveNetworkInfo().isAvailable() && manager.getActiveNetworkInfo().isConnected();
    }
}
