package com.digitalmount.sbertech.data.database;

import com.digitalmount.sbertech.data.model.CurrencyModel;

import java.util.List;

/**
 * Created by vadimkarpenko on 30.11.17.
 */

public interface IDatabase {
    List<CurrencyModel> getCurrencyModels();

    void updateCurrencyModels(List<CurrencyModel> data);

    void addValueModel(CurrencyModel model);

    int getSize();

    void clearDb();
}
