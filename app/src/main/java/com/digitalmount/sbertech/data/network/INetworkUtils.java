package com.digitalmount.sbertech.data.network;

/**
 * Created by Vadim Karpenko on 06.12.2017.
 */

public interface INetworkUtils {
    public boolean connectionEnabled();

}
