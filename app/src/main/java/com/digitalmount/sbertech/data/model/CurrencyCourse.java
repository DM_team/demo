package com.digitalmount.sbertech.data.model;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.List;

/**
 * Created by vadimkarpenko on 30.11.17.
 */

@Root(name = "ValCurs")
public class CurrencyCourse {

    @Attribute(name = "name")
    private String name;

    @ElementList(inline = true, name = "Valute")
    private List<CurrencyModel> currencyModels;

    public List<CurrencyModel> getValueModels() {
        return currencyModels;
    }
}
