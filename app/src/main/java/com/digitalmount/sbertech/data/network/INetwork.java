package com.digitalmount.sbertech.data.network;

/**
 * Created by vadimkarpenko on 30.11.17.
 */

public interface INetwork {
    void getCurrencyModels(OnNetworkLoad onSuccessLoad);
}
