package com.digitalmount.sbertech.data.database;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.digitalmount.sbertech.data.model.CurrencyModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vadimkarpenko on 30.11.17.
 */

public class Database implements IDatabase {
    private static final String TAG = "Database";
    private final DatabaseHelper helper;

    public Database(DatabaseHelper helper) {
        this.helper = helper;
    }


    @Override
    public List<CurrencyModel> getCurrencyModels() {
        SQLiteDatabase database = helper.getReadableDatabase();

        List<CurrencyModel> models = new ArrayList<>();


        String query = "SELECT * FROM " + DatabaseContract.TABLE_NAME;
        Cursor cursor = database.rawQuery(query, null);

        if (cursor.moveToFirst()) {
            do {
                String id = cursor.getColumnName(cursor.getColumnIndex(DatabaseContract.COLUMN_ID));
                String name = cursor.getString(cursor.getColumnIndex(DatabaseContract.COLUMN_NAME));
                String code = cursor.getString(cursor.getColumnIndex(DatabaseContract.COLUMN_CODE));
                String nominal = cursor.getString(cursor.getColumnIndex(DatabaseContract.COLUMN_NOMINAL));
                String value = cursor.getString(cursor.getColumnIndex(DatabaseContract.COLUMN_VALUE));
                String numCode = cursor.getString(cursor.getColumnIndex(DatabaseContract.COLUMN_NUM));

                CurrencyModel model = new CurrencyModel();
                model.setName(name);
                model.setCharCode(code);
                model.setId(id);
                model.setNominal(Integer.parseInt(nominal));
                model.setValue(value);
                model.setNumCode(Integer.parseInt(numCode));

                models.add(model);

            } while (cursor.moveToNext());
        }
        cursor.close();
        database.close();

        return models;
    }

    public void clearDb() {
        Log.d(TAG, "clearDb() called");
        SQLiteDatabase database = helper.getReadableDatabase();

        helper.onUpgrade(database, 1, 0);
    }

    @Override
    public void updateCurrencyModels(List<CurrencyModel> data) {
        SQLiteDatabase database = helper.getWritableDatabase();

        for (CurrencyModel model : data) {
            if (containsCurrencyModel(model)) {
                ContentValues values = new ContentValues();
                values.put(DatabaseContract.COLUMN_CODE, model.getCharCode());
                values.put(DatabaseContract.COLUMN_ID, model.getId());
                values.put(DatabaseContract.COLUMN_NAME, model.getName());
                values.put(DatabaseContract.COLUMN_NOMINAL, model.getNominal());
                values.put(DatabaseContract.COLUMN_VALUE, model.getValue());
                values.put(DatabaseContract.COLUMN_NUM, model.getNumCode());

                String selection = DatabaseContract.COLUMN_ID + " LIKE ?";
                String[] argsSelection = {String.valueOf(model.getNumCode())};

                int row = database.update(
                        DatabaseContract.TABLE_NAME,
                        values,
                        selection,
                        argsSelection
                );
            }
        }

        database.close();
    }

    @Override
    public void addValueModel(CurrencyModel model) {
        if (!containsCurrencyModel(model)) {
            SQLiteDatabase database = helper.getWritableDatabase();

            ContentValues values = new ContentValues();
            values.put(DatabaseContract.COLUMN_CODE, model.getCharCode());
            values.put(DatabaseContract.COLUMN_ID, model.getId());
            values.put(DatabaseContract.COLUMN_NAME, model.getName());
            values.put(DatabaseContract.COLUMN_NOMINAL, model.getNominal());
            values.put(DatabaseContract.COLUMN_VALUE, model.getValue());
            values.put(DatabaseContract.COLUMN_NUM, model.getNumCode());

            long row = database.insert(DatabaseContract.TABLE_NAME, null, values);
        }
    }

    private boolean containsCurrencyModel(CurrencyModel model) {
        for (CurrencyModel m : getCurrencyModels()) {
            if (model.getNumCode() == m.getNumCode()) {
                return true;
            }
        }

        return false;
    }

    @Override
    public int getSize() {
        return getCurrencyModels().size();
    }

}
