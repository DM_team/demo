package com.digitalmount.sbertech.data.network;

import com.digitalmount.sbertech.data.model.CurrencyModel;

import java.util.List;

/**
 * Created by Vadim Karpenko on 30.11.2017.
 */
public interface OnNetworkLoad {

    void onLoad(List<CurrencyModel> valueModels);

    void onError(String error);
}
