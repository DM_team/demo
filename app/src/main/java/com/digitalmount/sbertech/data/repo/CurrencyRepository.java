package com.digitalmount.sbertech.data.repo;

import com.digitalmount.sbertech.data.database.IDatabase;
import com.digitalmount.sbertech.data.model.CurrencyModel;
import com.digitalmount.sbertech.data.network.INetwork;
import com.digitalmount.sbertech.data.network.INetworkUtils;
import com.digitalmount.sbertech.data.network.Network;
import com.digitalmount.sbertech.data.network.OnLoadCallBack;
import com.digitalmount.sbertech.data.network.OnNetworkLoad;

import java.io.IOException;
import java.util.List;

/**
 * Created by vadimkarpenko on 30.11.17.
 */

public class CurrencyRepository implements ICurrencyRepository {
    private INetwork network;
    private final IDatabase database;

    private static ICurrencyRepository instance = null;
    private static final String TAG = CurrencyRepository.class.getSimpleName();
    private OnLoadCallBack onLoadCallBack;
    private boolean dataBaseUpdated;
    private INetworkUtils networkUtils;

    public CurrencyRepository(IDatabase database, INetworkUtils networkUtils) {
        this.networkUtils = networkUtils;

        try {
            network = new Network();
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.database = database;

    }

    public static ICurrencyRepository getInstance(IDatabase database, INetworkUtils networkUtils) {
        if (instance == null) instance = new CurrencyRepository(database, networkUtils);

        return instance;
    }


    @Override
    public void getCurrencyModels(OnLoadCallBack onLoadCallBack) {
        this.onLoadCallBack = onLoadCallBack;

        if (dataBaseUpdated) {
            pushUpdate(onLoadCallBack);
        } else {
            if (networkUtils.connectionEnabled()) {
                network.getCurrencyModels(new OnNetworkLoad() {
                    @Override
                    public void onLoad(List<CurrencyModel> valueModels) {

                        onLoadResp(valueModels);
                    }

                    @Override
                    public void onError(String error) {
                        onLoadCallBack.onError(error);
                    }
                });
            } else {
                if (database.getCurrencyModels().size() > 0) {
                    pushUpdate(onLoadCallBack);
                } else {
                    onLoadCallBack.onError("Can't load currency list because of no internet connection");
                }
            }
        }


    }

    private void pushUpdate(OnLoadCallBack onLoadCallBack) {
        onLoadCallBack.onSuccessLoad(database.getCurrencyModels());
    }

    private void onLoadResp(List<CurrencyModel> currencyModels) {

        database.clearDb();

        if (database.getSize() == 0) {
            addCurrencyModels(currencyModels);
        }

        dataBaseUpdated = true;
        pushUpdate(onLoadCallBack);
    }


    @Override
    public void addCurrencyModels(List<CurrencyModel> data) {
        for (CurrencyModel model : data) {
            database.addValueModel(model);
        }
    }


}
