package com.digitalmount.sbertech.data.network;

import java.io.IOException;

/**
 * Created by vadimkarpenko on 30.11.17.
 */

public class Network implements INetwork {

    private CurrencyLoader loader;

    public Network() throws IOException {
        loader = new CurrencyLoader();
    }

    @Override
    public void getCurrencyModels( OnNetworkLoad onSuccessLoad) {


        loader.getModelsList(onSuccessLoad);
    }
}
