package com.digitalmount.sbertech.utils;

import com.digitalmount.sbertech.data.model.CurrencyModel;

/**
 * Created by vadimkarpenko on 30.11.17.
 */

public class Converter {

    public static double solve(double sum, CurrencyModel from, CurrencyModel to) {
        double fromValue = Double.parseDouble(from.getValue().replace(",", "."));
        double toValue = Double.parseDouble(to.getValue().replace(",", "."));

        double sumInRubls = (sum * fromValue) / from.getNominal();

        double courseToAnother = to.getNominal() / toValue;

        return courseToAnother * sumInRubls;
    }
}
